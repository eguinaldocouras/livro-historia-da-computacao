[ ⬅ Voltar ](.../README.MD)

# Computação Móvel

## O que é?
A computação móvel é um termo popular e pode ser definida como o acesso à informação em qualquer lugar, a qualquer momento e com qualquer equipamento. Porém, temos também a computação pervasiva, que é o acesso à informação distribuída no ambiente de forma imperceptível e perceptível ao usuário[1]

## Usos:
 Apresenta muitas aplicações interessantes nas áreas de negócios, comércio eletrônico, telecomunicação, entretenimento, situações de emergência, medicina, entre outras. O usuário tem mobilidade, isto é, não precisa manter-se numa posição fixa, e as redes sem fio possibilitam a comunicação destes usuários com sistemas de software ou outros usuários.[2] 

  A cada dia, mais e mais pessoas aderem a computadores móveis devido a sua praticidade no dia a dia. Celulares, notebooks, fones de ouvido, mouse, teclado, todo o tipo de aparato que se torne de mais fácil de usar com a ausência de fios. Ainda mais faciltado pelo atual grande suporte de internet móvel.

  ## Limitações: 
  ### Hardware:
   Não tendo muito espaço disponível porque o tamanho e o peso acabam penalizando recursos computacionais como velocidade de processamento e capacidade de disco

 ### Risco de dano:
  Ao mesmo tempo que mobilidade se torna uma vantagem, tem efeito colateral. Tendo uma maior facilidade de dano, uma vez que ao estar  em movimento, tem risco de queda ou impacto. E também a possibilidade de furto, sendo muito mais acessível estando, por exemplo, na rua do que em sua casa ou seu ambiente de trabalho.

  ### Conectividade:
  Nem todos lugares tendo acesso a rede móvel, se torna limitado seu uso.

 ### Bateria:
  A bateria tendo um limite maximo de uso, após esse tempo ser necessário efetuar a sua recarga, transforma o aparelho em um aparelho com fio durante esse tempo, e pelo fato da bateria se desgastar com o tempo, alguns aparelhos tem uma vida útil menor por dependerem de suas baterias.

  ## Referências:
  Computação móvel, Publicado em: https://www.linux.ime.usp.br/~cef/mac499-05/monografias/patty/tecnica/computacaoMovel.html
 
  O QUE É COMPUTAÇÃO MÓVEL E COMO VOCÊ PODE FAZER USO DESSA TECNOLOGIA?;
  Publicado em: http://logicalminds.com.br/o-que-e-computacao-movel-e-como-voce-pode-fazer-uso-dessa-tecnologia/#:~:text=A%20computação%20móvel%20é%20um,imperceptível%20e%20perceptível%20ao%20usuário.

[ ⬅ Voltar ](.../README.MD)
 
 
