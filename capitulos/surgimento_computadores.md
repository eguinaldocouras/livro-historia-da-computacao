[ ⬅ Voltar ](.../README.MD)

# Surgimento dos primeiros computadores

o surgimento do primeiro computador foi no seculo XVII com a Pascaline, máquina calculadora feita por Blaise Pascal com o intuito de calcular impostos, ela era capaz de realizar contas de soma e subtração.
<p><a href="https://commons.wikimedia.org/wiki/File:Arts_et_Metiers_Pascaline_dsc03869.jpg#/media/File:Arts_et_Metiers_Pascaline_dsc03869.jpg"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/8/80/Arts_et_Metiers_Pascaline_dsc03869.jpg/1200px-Arts_et_Metiers_Pascaline_dsc03869.jpg" alt="Arts et Metiers Pascaline dsc03869.jpg" width = 300px></a><br>By David.Monniaux, <a href="http://creativecommons.org/licenses/by-sa/3.0/" title="Creative Commons Attribution-Share Alike 3.0">CC BY-SA 3.0</a>, <a href="https://commons.wikimedia.org/w/index.php?curid=186079">Link</a></p>

Todas essas máquinas, porém, estavam longe de ser um computador de uso geral, pois não eram programáveis. Isto quer dizer que a entrada era feita apenas de números, mas não de instruções a respeito do que fazer com os números. 

tear programavel:
em 1801, o costureiro Joseph Marie Jacquard desenvolveu a primeira maquina programavel, com o objetivo de recortar tecidos de forma automática.

Tal mecanismo foi chamado de Tear Programável, pois aceitava cartões com entrada do sistema. Dessa maneira, Jacquard perfurava o cartão com o desenho desejado e a máquina o reproduzia no tecido. A partir daquele momento, muitos esquemas foram influenciados(como a maquina de babbage).
<p><a href="https://commons.wikimedia.org/wiki/File:Jacquard_loom_p1040320.jpg#/media/Ficheiro:Jacquard_loom_p1040320.jpg"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Jacquard_loom_p1040320.jpg/1200px-Jacquard_loom_p1040320.jpg" alt="Jacquard loom p1040320.jpg" width=300px></a><br>Por &lt;a href="//commons.wikimedia.org/wiki/User:David.Monniaux" title="User:David.Monniaux"&gt;David Monniaux&lt;/a&gt; - &lt;span class="int-own-work" lang="pt"&gt;Obra do próprio&lt;/span&gt;, <a href="http://creativecommons.org/licenses/by-sa/3.0/" title="Creative Commons Attribution-Share Alike 3.0">CC BY-SA 3.0</a>, <a href="https://commons.wikimedia.org/w/index.php?curid=959006">Hiperligação</a></p>

Após um período, em 1837, Babbage lançou uma nova máquina que era capaz de calcular funções de diversas naturezas (trigonometria, logaritmos) de forma muito simples. chamada de Engenho Analítico (Máquina Analítica), que aproveitava todos os conceitos do Tear Programável, como o uso dos cartões. Além disso, instruções e comandos podiam ser informados pelos cartões, fazendo uso de registradores primitivos. A precisão chegava a 50 casas decimais.

Máquina de Hollerith

O conceito de cartões desenvolvido no Tear Programável foi muito útil para a realização do censo de 1890 nos Estados Unidos. Na ocasião, Hermann Hollerith desenvolveu uma máquina que acelerou todo o processo de computação de dados.

Em vez da clássica caneta para marcar X em "sim" e "não", os agentes do censo perfuravam as opções nos cartões. Uma vez que os dados foram coletados, o processo de computação da informação demorou aproximadamente um terço do comum.
<p><a href="https://commons.wikimedia.org/wiki/File:HollerithMachine.CHM.jpg#/media/Ficheiro:HollerithMachine.CHM.jpg"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/4/4e/HollerithMachine.CHM.jpg/1200px-HollerithMachine.CHM.jpg" alt="HollerithMachine.CHM.jpg" width = 300px></a><br>Por &lt;a rel="nofollow" class="external text" href="https://www.flickr.com/people/44124384537@N01"&gt;Adam Schuster&lt;/a&gt; - &lt;a href="//commons.wikimedia.org/wiki/Flickr" class="mw-redirect" title="Flickr"&gt;Flickr&lt;/a&gt;: &lt;a rel="nofollow" class="external text" href="https://www.flickr.com/photos/44124384537@N01/411109339"&gt;Proto IBM&lt;/a&gt;, <a href="https://creativecommons.org/licenses/by/2.0" title="Creative Commons Attribution 2.0">CC BY 2.0</a>, <a href="https://commons.wikimedia.org/w/index.php?curid=13310425">Hiperligação</a></p>

Aproveitando todo o sucesso ocasionado por sua máquina, Hollerith fundou a própria empresa, a Tabulation Machine Company, em 1896. Após algumas fusões com outras companhias e anos no comando do empreendimento, Hoolerith faleceu. Quando um substituto assumiu o lugar, em 1916, o nome da empresa foi alterado para Internacional Business Machine, a mundialmente famosa IBM.

Computadores valvulados:
Ao contrario dos computadores serem programados com cartões perfurados, a tecnologia evoluiu e mudou para valvulas. sendo assim computadores com funcionamento totalmente eletronico.
Eniac

<p><a href="https://commons.wikimedia.org/wiki/File:Two_women_operating_ENIAC.gif#/media/Ficheiro:Two_women_operating_ENIAC.gif"><img src="https://upload.wikimedia.org/wikipedia/commons/3/3b/Two_women_operating_ENIAC.gif" alt="Two women operating ENIAC.gif" width = 300px></a><br>Por &lt;a href="//commons.wikimedia.org/wiki/United_States_Army" class="mw-redirect" title="United States Army"&gt;United States Army&lt;/a&gt; - Image from &lt;a rel="nofollow" class="external autonumber" href="http://ftp.arl.army.mil/ftp/historic-computers/gif/eniac7.gif"&gt;[1]&lt;/a&gt;, Domínio público, <a href="https://commons.wikimedia.org/w/index.php?curid=978783">Hiperligação</a></p>

Em 1946 ocorreu uma revolução no mundo da computação com o lançamento do Electrical Numerical Integrator and Calculator (ENIAC), desenvolvido pelos cientistas norte-americanos John Eckert e John Mauchly. Essa máquina era em torno de mil vezes mais rápida que qualquer outra da época. Ao contrario das maquinas da epoca(que eram com reles eletromecanicos), ela funcionava com valvulas.

Segunda geração:
Na segunda geração, houve a substituição das válvulas eletrônicas por transístores, o que diminuiu muito o tamanho e a temperatura do hardware. A tecnologia de circuitos impressos também foi criada, evitando que fios e cabos elétricos ficassem espalhados. É possível dividir os computadores dessa geração em duas grandes categorias: supercomputadores e minicomputadores.

IBM 7030
<p><a href="https://commons.wikimedia.org/wiki/File:IBM_7030-CNAM_22480-IMG_5115-gradient.jpg#/media/File:IBM_7030-CNAM_22480-IMG_5115-gradient.jpg"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/4/4c/IBM_7030-CNAM_22480-IMG_5115-gradient.jpg/1200px-IBM_7030-CNAM_22480-IMG_5115-gradient.jpg" alt="IBM 7030-CNAM 22480-IMG 5115-gradient.jpg" width = 300px></a><br>By Rama, <a href="https://creativecommons.org/licenses/by-sa/3.0/fr/deed.en" title="Creative Commons Attribution-Share Alike 3.0 fr" width = 300px>CC BY-SA 3.0 fr</a>, <a href="https://commons.wikimedia.org/w/index.php?curid=70342818">Link</a></p>

O IBM 7030, foi um dos primeiros computadores popularizados entre as empresas, pois ele era muito mais compacto comparado aos computadores da epoca(cerca do tamanho de uma sala comum) e ele poderia ser programado em Fortran, Cobol e Algol.

Terceira geração:
A terceria geração é marcada por possuir computadores com suporte de dispositivos de entrada e saida modernos, como discos e fitas de armazenamento e imprimir resultados em papel.
<p><a href="https://commons.wikimedia.org/wiki/File:360-91-panel.jpg#/media/File:360-91-panel.jpg"><img src="https://upload.wikimedia.org/wikipedia/commons/9/9e/360-91-panel.jpg" alt="360-91-panel.jpg" width = 300px></a><br>By Unknown author - &lt;a href="https://en.wikipedia.org/wiki/NASA" class="extiw" title="en:NASA"&gt;NASA&lt;/a&gt;, Public Domain, <a href="https://commons.wikimedia.org/w/index.php?curid=931293">Link</a></p>

Quarta geração(de 1970 até atualmente):
O inicio da quarta geração surgiu com o advento dos microprocessadores e computadores pessoais, com as maquinas possuindo tamanhos muito menores que as do passado, foi possivel tambem aumentar a capacidade de processamento de milhões para bilhões de calculos por segundo, permitindo fazer algoritmos mais complexos.

Altair 8800
<p><a href="https://commons.wikimedia.org/wiki/File:Altair_8800.jpg#/media/Ficheiro:Altair_8800.jpg"><img src="https://upload.wikimedia.org/wikipedia/commons/1/19/Altair_8800.jpg" alt="Altair 8800.jpg" width = 300px></a><br><a href="http://creativecommons.org/licenses/by-sa/3.0/" title="Creative Commons Attribution-Share Alike 3.0">CC BY-SA 3.0</a>, <a href="https://commons.wikimedia.org/w/index.php?curid=254104">Hiperligação</a></p>

O Altair 8800, era um excelente computador, porém dificil de ser utilizado pois ele nao possuia interface grafica, apenas cartões de entrada e saida de dados. Logo em seguida surge Bill Gates, solucionando esse problema e criando o Apple I, um computador com entrada de dados por meio de teclado e saida por um monitor de tubo. Com o sucesso das vendas, Bill decide lançar Lisa e Macintosh, computadores muito semelhantes com o que utilizamos hoje em dia, pois eles pussuiam mouse, teclado, monitor, e uma interface grafica com aréa de trabalho e estrutura de pastas.
# Referencias:
- https://pt.wikipedia.org/wiki/Computador#As_primeiras_m%C3%A1quinas_de_computar
- https://www.tecmundo.com.br/tecnologia-da-informacao/1697-a-historia-dos-computadores-e-da-computacao.htm

[ ⬅ Voltar ](.../README.MD)
