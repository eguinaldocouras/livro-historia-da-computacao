[ ⬅ Voltar ](.../README.md)

# Alan Turing
<img src="https://aventurasnahistoria.uol.com.br/media/_versions/personagem/turignmamfmdaf_widelg.jpg" width="300">

Alan Turing, (nascido em 1912) foi um grande matemático britânico, que por muitos visto como o pai da computação, teve um papel significativo durante a Segunda Guerra Mundial.

Desde muito cedo, já apresentava um talento excepcional para matemática e as demais ciências. De modo que, em 1934, se formou na Universidade de Cambridge, no curso de Matemática.

# Trabalho e Contribuição Durante a Segunda Guerra

A carreira de sucesso de Turing se iniciou após se juntar a organização britânica de inteligência. Durante seu tempo na mesma, trabalhou em busca da solução das mensagens criptografadas que eram geradas pela Enigma, uma máquina de criptografia criada pela Alemanha nazista, a fim dos soldados se comunicarem entre si sem que a mensagem fosse lida, caso interceptada pelo exército britânico ou aliados.
Diante disso, Alan foi capaz de desenvolver a bomba eletromecânica, ou Bombe, que decifrou a máquina Enigma.

<img src="http://www.invivo.fiocruz.br/media/bombep.jpg" width="300">

Além disso, também desenvolveu o Automatic Computing Engine (ACE), foi o primeiro projeto de computador, que em 1950 foi capaz de rodar seu primeiro programa.

<img src="https://coimages.sciencemuseumgroup.org.uk/images/34/674/medium_1956_0152__0001_.jpg" width="300">

Também criou o Teste de Turing que, basicamente testava a competência de uma máquina em demonstrar comportamento próximo ao do ser humano. Neste caso, duas pessoas e o computador iniciavam uma conversa, se uma dessas pessoas não conseguisse distinguir a pessoa e a máquina, então o teste seria bem sucedido. 

# Morte

Suas incomparáveis contribuições foram irrelevantes para seus perseguidores, o governo britânico, pelo simples fato de ser homossexual, o que na época era considerado um crime, e infelizmente, foi privado de acompanhar o andamento de seus trabalhos, e em 1952, foi forçado a aceitar o procedimento de castração química, para continuar em liberdade.

E por fim, no ano de 1954, Alan Turing acabou falecendo com 41 anos, em decorrência de intoxicação por cianeto, tudo indica de que ele cometeu suicídio.

# Referências
- http://www.invivo.fiocruz.br/cgi/cgilua.exe/sys/start.htm?infoid=1370&sid=7
- https://pt.wikipedia.org/wiki/Alan_Turing
- https://www.ebiografia.com/alan_turing/
- https://aventurasnahistoria.uol.com.br/noticias/reportagem/alan-turing-matematico-considerado-pai-computacao.phtml


[ ⬅ Voltar ](.../README.md)