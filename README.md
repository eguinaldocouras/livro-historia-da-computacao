# Introdução a Engenharia de Computação

Este livro está sendo escrito pelos alunos do Curso de Engenharia da Computação da Universidade Tecnológica do Paraná. O livro é faz parte da disciplina de Introdução a Engenharia e os alunos que participaram da edição estão [listados abaixo](#Autores).Redme

## Índice
1. [Alan Turing](capitulos/alan_turing.md)
1. [Surgimento das Calculadoras Mecânicas](capitulos/surgimento_das_calculadoras_mecanicas.md)
1. [Primeiros Computadores](capitulos/surgimento_computadores.md)
1. [Evolução dos Computadores Pessoais e sua Interconexão](capitulos/evolucoes_comp_pessoas.md)
1. [Computação Móvel](capitulos/computacao_movel.md)
1. [Computador de redstone](capitulos/redstone computer.md)
1. [Computação Quântica](capitulos/computacao_quantica.md)




## Autores
Esse livro foi escrito por:

| Avatar | Nome | Nickname | Email |
| ------ | ---- | -------- | ----- |
<img src="https://gitlab.com/uploads/-/system/user/avatar/8279861/avatar.png?width=400" width = 100> | Eguinaldo Couras | Gui.oliveira | [eguinaldocouras@gmail.com](mailto:eguinaldocouras@gmail.com)
| <img src="https://gitlab.com/uploads/-/system/user/avatar/8279920/avatar.png?width=400" width = 100>  | Arthur Klipp | Hellraiser | [arthurklipp@alunos.utfpr.edu.br](mailto:arthurklipp@alunos.utfpr.edu.br)
| <img src="https://gitlab.com/uploads/-/system/user/avatar/8231107/avatar.png?width=400" width = 100>  | Julio V Perin| JulioPerin | [julioperin@live.com](mailto:julioperin@live.com)
<img src="https://gitlab.com/uploads/-/system/user/avatar/5669368/avatar.png?width=400" width = 100> | Samuel H Fabricio | SamuelFabricio | [samuel.1995@alunos.utfpr.edu.br](mailto:samuel.1995@alunos.utfpr.edu.br)
<img src="https://gitlab.com/uploads/-/system/user/avatar/8623848/avatar.png?width=400" width = 100> | Maria Clara de Lima | Lclara | [mariaclaralima@alunos.utfpr.edu.br](mailto:mariaclaralima@alunos.utfpr.edu.br)
